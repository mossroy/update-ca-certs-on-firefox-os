#!/bin/bash

# Requirements :
#   - adb installed and configured for the phone, with root access
#   - certutil installed ('sudo apt install libnss3-tools' on Debian/Ubuntu)

if ! [ -x "$(command -v certutil)" ]; then
	echo "certutil is not installed" >&2
	exit 1
fi

if ! [ -x "$(command -v adb)" ]; then
	echo "adb is not installed" >&2
	exit 1
fi

echo --- Stop B2G

adb shell stop b2g || exit 1

PHONE_PROFILE_DIR=$(adb shell ls -d /data/b2g/mozilla/*.default | tr -d '\r')

echo --- Download certificates from $PHONE_PROFILE_DIR of the phone

adb pull $PHONE_PROFILE_DIR/cert9.db
adb pull $PHONE_PROFILE_DIR/key4.db
adb pull $PHONE_PROFILE_DIR/pkcs11.txt

echo --- Make copies of these files, with .original suffix, if they do not already exist

if [ ! -f cert9.db.original ]; then cp cert9.db cert9.db.original; fi
if [ ! -f key4.db.original ]; then cp key4.db key4.db.original; fi
if [ ! -f pkcs11.txt ]; then cp pkcs11.txt pkcs11.txt.original; fi

echo --- Add ISRG Root X1 CA
echo --- Press Enter when asked for a password

certutil -d sql:. -W
certutil -d sql:. -A -n "ISRG_root_X1" -t "C,C,TC" -i isrgrootx1.pem

echo --- Replace certificates in $PHONE_PROFILE_DIR of the phone

adb push cert9.db $PHONE_PROFILE_DIR
adb push key4.db $PHONE_PROFILE_DIR
adb push pkcs11.txt $PHONE_PROFILE_DIR

echo --- Restart B2G

adb shell start b2g
