# Update CA certificates on Firefox OS

A script to update the SSL CA certificates on a Firefox OS device to workaround the expiry on 2021-09-30 of a LetsEncrypt certificate chain, that blocks access to a lot of current websites.

Tested on Ubuntu 20.04 and Debian 11, with a ZTE Open C FR device plugged on the computer.

It probably works on some other Firefox OS devices, and some other operating systems (on the computer side).

In any case, you use this script at your own risk!

Requirements:

  - adb installed and configured for the phone. You can follow https://web.archive.org/web/20161226015708/https://developer.mozilla.org/fr/docs/B2G_OS/D%C3%A9boguer/Installer_ADB and https://web.archive.org/web/20160303174010/https://developer.mozilla.org/fr/Firefox_OS/Prerequis_pour_construire_Firefox_OS#Pour_Linux_.3A_configurer_la_r.C3.A8gle_udev_li.C3.A9e_au_t.C3.A9l.C3.A9phone
  - root access to the phone via adb
  - certutil installed. On Debian/Ubuntu, it can be installed with :


    sudo apt install libnss3-tools

Procedure:

  - Plug the device on your computer
  - run `adb devices` to check your phone is listed. If it's not, you might need to enable ADB in the "developer settings" of your phone
  - run `adb root` to check you have root access
  - run the script

Sources :

  - https://letsencrypt.org/2021/10/01/cert-chaining-help.html
  - https://docs.certifytheweb.com/docs/kb/kb-202109-letsencrypt/

The pem file comes from https://letsencrypt.org/certs/isrgrootx1.pem
